﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ClickObject : MonoBehaviour
{
    public static ClickObject instance;
    public LayerMask toolsMask = 1;
    public Camera cameraMain;
    public Camera workCamera;
    public Camera shopCamera;
    public Camera homeCamera;
    public Camera scoreCamera;
    public PaintExample _PaintExample;
    public GameObject shoePrefab;
    public GameObject scoreGo;
    public Text TextA;
    public Text TextB;
    public Text TextC;
    public Text TextD;
    public Text nextBtnText;
    public GameObject exitBtn;
    public Light _Light;
    public Light homeLight;

    public Text dayText;
    public GameObject dayBG;
    
    public GameObject scoreShoePos;

    public GameObject f1Object;

    GameObject clickNow;
    RaycastHit hit;
    Ray ray;

    public ToolsEnum mainEnum;
    public ShoeState shoeEnum;

    public UIGameA _UIGameA;
    Shoe fixedShoeNow;
    public Text shoeTime3D;


    public GameObject homeBtn;
    public GameObject shopBtn;
    public GameObject workBtn;
    public GameObject scoreBtn;

    public GameObject dialog;
    public Text dialogText;

    Animator tempAnimator;
    public Animator bookAni;

    public List<GameObject> doneList;
    public Texture2D cursorTextureOn;
    public Texture2D cursorTextureOff;

    public GameObject bottomGameUI;
    public Image bottomGameTopArror;
    public Image bottomGameRightArror;

    public Sprite gSprite;
    public Sprite wSprite;

    public Animator kakoAnimator;

    public enum ToolsEnum
    {
        None = 0, 
        Hammer = 1,
        Bottom = 2,
        Brush = 3,
        Del = 4,
        Book = 5,
    }

    public enum ShoeState
    {
        None = 0,
        HammerDone = 1,
        BottomDone = 2,
    }

    Vector3 oldCameraPos;
    void SetCamaerPos()
    {
        oldCameraPos = cameraMain.transform.position;
    }

    bool isf1Fly = false;
    public void OnF1End()
    {
        f1Object.SetActive(true);
        isf1Fly = false;
    }

    //產生鞋子
    public void SetShoe()
    {
        kakoAnimator.enabled = false;
        GameManager.instance.CreatParticle(transform.position, "S_Tool");
        Debug.Log("SetShoe");
        isNoShoe = false;
        GameObject go = Instantiate(shoePrefab) as GameObject;
        go.SetActive(true);
        fixedShoeNow = go.GetComponent<Shoe>();
    }

    //刪掉鞋子
    bool isNoShoe = false;
    float delTimer = 1;
    public void DelShoeNow() 
    {
        GameManager.instance.CreatParticle(transform.position, "S_ShoeDone");
        mainEnum = ToolsEnum.Del;
        fixedShoeNow.GetComponent<Animator>().enabled = true;

        if (fixedShoeNow.newBottom.transform.parent != fixedShoeNow.transform)
        {
            fixedShoeNow.newBottom.gameObject.SetActive(false);
        }

        doneList.Add(fixedShoeNow.gameObject);
    }

    void ResetShoe()
    {
        kakoAnimator.enabled = true;
        fixedShoeNow.GetComponent<Animator>().enabled = false;
        delTimer = 1;
        mainEnum = ToolsEnum.None;
        shoeEnum = ShoeState.None;
        isPlayBottomGame = false;
        isStartBottomGame = false;
        isUsingHammer = false;
        isPlayingBottom = false;
        clickNow = null;
        tempAnimator = null;
        isNoShoe = true;
        fixedShoeNow = null;
    }

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    bool isDayShowUIClose = false;
    private void Start()
    {
        Cursor.SetCursor(cursorTextureOff, Vector2.zero, CursorMode.Auto);
        isDayShowUIClose = false;
        scoreBtn.SetActive(false);
        workBtn.SetActive(false);
        homeBtn.SetActive(false);
        shopBtn.SetActive(false);
        dayBG.SetActive(true);
        isNoShoe = true;
        dayText.text = "Day " + GameManager.instance.Day.ToString();
        GameManager.instance.SetTimer();
        doneList = new List<GameObject>();
        SetCamaerPos();
    }
    void Update()
    {
        if (!isDayShowUIClose) return;

        if (cameraMain == shopCamera)
        {
            CheckShopClick();
        }

        if (cameraMain == workCamera)
        {
            CheckWorkCamera();
        }

        if (cameraMain == workCamera)
        {
            if(mainEnum == ToolsEnum.None)
            {
                if(!shopBtn.activeSelf)
                {
                    shopBtn.SetActive(true);
                }
                if (!homeBtn.activeSelf)
                {
                    homeBtn.SetActive(true);
                }
            }
            else
            {
                if (shopBtn.activeSelf)
                {
                    shopBtn.SetActive(false);
                }

                if (!homeBtn.activeSelf)
                {
                    homeBtn.SetActive(false);
                }
            }
        }
        
        if (isExit)
        {
            CheckClickDeskScore();
            return;
        }

        if(Input.GetKeyDown("w"))
        {
            //shoeEnum = ShoeState.BottomDone;
        }
        if(mainEnum == ToolsEnum.Del)
        {
            delTimer -= Time.deltaTime;
            
            if(delTimer <= 0)
            {
                ResetShoe();
            }
        }
        CheckClick();
        PlayBottomGame();
    }

    //玩黏鞋底遊戲
    bool isPlayBottomGame = false;
    bool isStartBottomGame = false;
    void PlayBottomGame()
    {
        if (!isPlayBottomGame) return;

        if (isStartBottomGame) return;

        bottomGameUI.SetActive(true);
        isStartBottomGame = true;
        fixedShoeNow.OnPlayBottomGame();

    }

    void CheckWorkCamera()
    {
        ray = cameraMain.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 100.0f))
        {
            if(mainEnum == ToolsEnum.None)
            {
                if (hit.transform.tag == "Done" && isNoShoe)
                {
                    Cursor.SetCursor(cursorTextureOn, Vector2.zero, CursorMode.Auto);
                    return;
                }

                else if (!isNoShoe)
                {
                    if (hit.transform.tag == "Hammer")
                    {
                        Cursor.SetCursor(cursorTextureOn, Vector2.zero, CursorMode.Auto);
                        return;
                    }
                    else if (hit.transform.tag == "Book")
                    {
                        Cursor.SetCursor(cursorTextureOn, Vector2.zero, CursorMode.Auto);
                        return;
                    }
                    else if (hit.transform.tag == "Brush")
                    {
                        Cursor.SetCursor(cursorTextureOn, Vector2.zero, CursorMode.Auto);
                        return;
                    }
                    else if (shoeEnum == ShoeState.HammerDone && hit.transform.tag == "Bottom")
                    {
                        Cursor.SetCursor(cursorTextureOn, Vector2.zero, CursorMode.Auto);
                        return;
                    }
                    else if (hit.transform.tag == "Done" && shoeEnum == ShoeState.BottomDone && mainEnum == ToolsEnum.None)
                    {
                        Cursor.SetCursor(cursorTextureOn, Vector2.zero, CursorMode.Auto);
                        return;
                    }
                }
            }
        }
        Cursor.SetCursor(cursorTextureOff, Vector2.zero, CursorMode.Auto);
    }

    void CheckShopClick()
    {
        int tempPrice = 0;
        int index = 0;

        if (isDayShowUIClose)
        {
            ray = cameraMain.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                if (hit.transform.tag == "ShopItem")
                    Cursor.SetCursor(cursorTextureOn, Vector2.zero, CursorMode.Auto);
                else
                {
                    Cursor.SetCursor(cursorTextureOff, Vector2.zero, CursorMode.Auto);
                }

            }
            
        }

        if (Input.GetMouseButtonDown(0))
        {
            ray = cameraMain.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                if (hit.transform.tag == "ShopItem" )
                {
                    
                    if (hit.transform.name == "ShopItem1")
                    {
                        tempPrice = 35000;
                        index = 0;
                    }
                    else if (hit.transform.name == "ShopItem2")
                    {
                        tempPrice = 66000;
                        index = 1;
                    }
                    else if(hit.transform.name == "ShopItem3")
                    {
                        tempPrice = 46000;
                        index = 2;
                    }
                    else if(hit.transform.name == "ShopItem4")
                    {
                        tempPrice = 31800;
                        index = 3;
                    }
                    else if(hit.transform.name == "ShopItem5")
                    {
                        tempPrice = 30000;
                        index = 4;
                    }
                    else if(hit.transform.name == "ShopItem6")
                    {
                        tempPrice = 56500;
                        index = 5;
                    }

                    if(GameManager.instance.Coin >= tempPrice)
                    {
                        GameManager.instance.CreatParticle(transform.position, "S_Buy");
                        GameManager.instance.Coin -= tempPrice;
                        GameManager.instance.UpdateCoin();
                        GameManager.instance.buyIndex = index;
                        GameManager.instance.butItem.Add(hit.transform.name);
                        Destroy(hit.transform.gameObject);
                    }
                    else
                        GameManager.instance.CreatParticle(transform.position, "S_ClickShopItem");
                }
            }
        }
        
    }

    void CheckClick()
    {
        if (Input.GetMouseButton(0))
        {
            ray = cameraMain.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100.0f, toolsMask))
            {
                if (hit.transform.tag == "Hammer" && mainEnum == ToolsEnum.None)
                {
                    Debug.Log("Click Hammer");
                    clickNow = hit.transform.gameObject;
                    mainEnum = ToolsEnum.Hammer;
                    UseHammer(hit.transform.gameObject);
                }

                //敲掉鞋底才能玩
                if (shoeEnum == ShoeState.HammerDone && hit.transform.tag == "Bottom" && mainEnum == ToolsEnum.None )
                {
                    Debug.Log("Click Bottom");
                    clickNow = hit.transform.gameObject;
                    mainEnum = ToolsEnum.Bottom;
                    OnMoveCameraTop();
                    isPlayBottomGame = true;
                }

                
                if (hit.transform.tag == "Brush" && mainEnum == ToolsEnum.None && !isNoShoe)
                {
                    Debug.Log("Click Brush");
                    clickNow = hit.transform.gameObject;
                    mainEnum = ToolsEnum.Brush;
                    UseBrush(hit.transform.gameObject);
                    _PaintExample.enabled = true;
                }

                //丟到桶子
                if (hit.transform.tag == "Done" && shoeEnum == ShoeState.BottomDone && mainEnum == ToolsEnum.None)
                {
                    Debug.Log("DelShoeNow");
                    DelShoeNow();
                }

                if(hit.transform.tag == "Done" && isNoShoe)
                {
                    
                    SetShoe();
                }


                if (hit.transform.tag == "Book" && mainEnum == ToolsEnum.None && !isf1Fly)
                {
                    
                    mainEnum = ToolsEnum.Book;
                    isf1Fly = true;
                    hit.transform.gameObject.GetComponent<Animator>().SetBool("isBook" , true);
                    GameManager.instance.CreatParticle(transform.position, "S_Book");
                }

            }

            if(Physics.Raycast(ray, out hit, 100.0f))
            {
                //旋轉鞋子
                if (hit.transform.tag == "Desk" && mainEnum == ToolsEnum.Brush)
                {
                    fixedShoeNow.transform.Rotate(0, Time.deltaTime * 150, 0, Space.Self);
                    tempAnimator.SetBool("isUsing", false);
                }
                else if (mainEnum == ToolsEnum.Brush)
                {
                    if (tempAnimator != null)
                    {
                        fixedShoeNow.OnScoreC();
                        tempAnimator.SetBool("isUsing", true);
                    }
                }
            }

            

        }
        else
        {
            if (mainEnum == ToolsEnum.Brush)
            {
                if (tempAnimator != null)
                {
                    tempAnimator.SetBool("isUsing", false);
                }
            }
        }

        if(Input.GetMouseButtonDown(1))
        {
            if(mainEnum == ToolsEnum.Hammer && !isUsingHammer)
            {
                if(tempAnimator.GetCurrentAnimatorStateInfo(0).IsName("HamamerTakeOff"))
                    UseHammer(clickNow, false);
            }

            if (mainEnum == ToolsEnum.Bottom && !isPlayBottomGame)
            {
                bottomGameUI.SetActive(false);
                OnMoveCameraMain();
                mainEnum = ToolsEnum.None;
            }

            if (mainEnum == ToolsEnum.Brush)
            {
                UseBrush(clickNow , false);
            }

            //書飛到定點了
            if(mainEnum == ToolsEnum.Book && !isf1Fly)
            {
                OnClickF1Back();
            }

            
        }

        //胡稿區
        if(Input.GetMouseButtonUp(0))
        {
            if(Physics.Raycast(ray, out hit, 100.0f))
            {
                if (hit.transform.tag == "Light" && mainEnum == ToolsEnum.None)
                {
                    GameManager.instance.CreatParticle(transform.position, "S_Light");
                    if (_Light.intensity == 2)
                        _Light.intensity = 4;
                    else
                        _Light.intensity = 2;
                }
            }

            
        }

        

    }

    void CheckClickDeskScore()
    {
        if (doneList.Count <= shoeCount) return;
        if (Input.GetMouseButton(0))
        {
            ray = cameraMain.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                //旋轉鞋子 左右
                if (hit.transform.tag == "Desk")
                {
                    doneList[shoeCount].transform.Rotate(0, Time.deltaTime * 150, 0, Space.Self);
                }
            }
        }
        if (Input.GetMouseButton(1))
        {
            ray = cameraMain.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                //旋轉鞋子 上下
                if (hit.transform.tag == "Desk")
                {
                    doneList[shoeCount].transform.Rotate(0, 0f, Time.deltaTime * 150, Space.Self);
                }
            }
        }
    }



    void UseHammer(GameObject _targetObject, bool isUse = true)
    {
        GameManager.instance.CreatParticle(transform.position , "S_Tool");
        tempAnimator = _targetObject.transform.parent.GetComponent<Animator>();
        if (isUse)
        {
            tempAnimator.SetBool("isTakeOff", true);
            _UIGameA.gameObject.SetActive(true);
            _UIGameA.OnStart();
        }
        else
        {
            tempAnimator.SetBool("isTakeOff", false);
            mainEnum = ToolsEnum.None;
            clickNow = null;
            tempAnimator = null;
            _UIGameA.gameObject.SetActive(false);
        }
    }

    bool isUsingHammer = false;
    public void OnHammerUsing()
    {
        isUsingHammer = true;
        if(tempAnimator != null)
        {
            tempAnimator.SetBool("isUsing", true);
        }
    }
    public void OnHammerGo()
    {
       // tempAnimator.SetBool("isUsing", false);
        tempAnimator.Play("HamamerGo", 0 , 0);

        OnHammerUsed();
    }
    public void OnHammerUsed()
    {
        isUsingHammer = false;
        
        if (tempAnimator != null)
        {
            tempAnimator.SetBool("isUsing", false);
        }

        if(shoeEnum == ShoeState.None && fixedShoeNow != null)
        {
            fixedShoeNow.scoreA -= 10;
        }
    }

    public void OnHammerGoMain() //敲到的一瞬間
    {
        GameManager.instance.CreatParticle(transform.position, "S_Damage");
        Debug.Log("敲到的一瞬間");
        if(fixedShoeNow != null)
            fixedShoeNow.OnFix();
       
    }

    //黏鞋底
    bool isPlayingBottom = false;
    public void OnMoveCameraTop()
    {
        cameraMain.GetComponent<Animator>().Play("moveTop" ,0 , 0);
    }
    public void OnMoveCameraMain()
    {
        cameraMain.GetComponent<Animator>().Play("Main", 0, 0);
    }

    public void OnBottomGameEnd()
    {
        isPlayBottomGame = false;
        ClickObject.instance.shoeEnum = ClickObject.ShoeState.BottomDone;
    }

    //上色
    void UseBrush(GameObject _targetObject, bool isUse = true)
    {
        if (fixedShoeNow == null) return;
        GameManager.instance.CreateSoundExample();
        tempAnimator = _targetObject.GetComponent<Animator>();
        if (isUse)
        {
            tempAnimator.SetBool("isTake", true);
            fixedShoeNow.transform.Rotate(0, 0.0f, 180f, Space.Self);
        }
        else
        {
            _PaintExample.enabled = false;
            tempAnimator.SetBool("isTake", false);
            fixedShoeNow.transform.Rotate(0, 0.0f, -180f, Space.Self);
            mainEnum = ToolsEnum.None;
            clickNow = null;
            tempAnimator = null;

        }
    }

    //按下離開時 開始結算
    bool isExit = false;
    int shoeCount = 0;
    public void OnExitShoe()
    {
        if (isExit) return;
        exitBtn.SetActive(false);
        if (delTimer > 0 && delTimer < 1)
        {
            ResetShoe();
        }
        if(fixedShoeNow != null)
        {
            fixedShoeNow.gameObject.SetActive(false);
        }
        isExit = true;
        scoreGo.SetActive(true);
        OnShowDetail();
    }

    //顯示數字
    void OnShowDetail()
    {
        if (doneList.Count > shoeCount)
        {
            doneList[shoeCount].transform.position = scoreShoePos.transform.position;
            doneList[shoeCount].transform.localScale = new Vector3(45,45,45);

            Shoe tempShoe = doneList[shoeCount].GetComponent<Shoe>();
            TextA.text = tempShoe.scoreA.ToString() + " %";
            TextB.text = tempShoe.scoreB.ToString() + " %";
            TextC.text = tempShoe.scoreC.ToString() + " %";

            int coin = (tempShoe.scoreA + tempShoe.scoreB + tempShoe.scoreC) * 10;
            GameManager.instance.CoinAdd(coin);
            GameManager.instance.UpdateCoin();

            TextD.text = "+$" + coin.ToString() ;
        }

        if(doneList.Count <= shoeCount+1)
        {
            nextBtnText.text = "Exit";
        }
    }

    //按下看下一雙
    public void OnNext()
    {
        if (doneList.Count > shoeCount)
            doneList[shoeCount].SetActive(false);

        if (doneList.Count <= shoeCount + 1)
        {
            GameManager.instance.ChangeScene("Work");
        }

        shoeCount += 1;
        OnShowDetail();

    }

    public void OnGoShop()
    {
        if(mainEnum != ToolsEnum.None)
        {
            return;
        }
        Cursor.SetCursor(cursorTextureOff, Vector2.zero, CursorMode.Auto);
        workBtn.SetActive(true);
        shopBtn.SetActive(false);
        GameManager.instance.CreatParticle(transform.position , "S_Shop");
        cameraMain = shopCamera;
        CloseAll();
    }

    public void OnWork()
    {
        Cursor.SetCursor(cursorTextureOff, Vector2.zero, CursorMode.Auto);
        GameManager.instance.CreatParticle(transform.position, "S_GoHome");
        workBtn.SetActive(false);
        shopBtn.SetActive(true);
        cameraMain = workCamera;
        CloseAll();
    }

    public void OnHome()
    {
        bottomGameUI.SetActive(false);
        Cursor.SetCursor(cursorTextureOff, Vector2.zero, CursorMode.Auto);
        GameManager.instance.CreatParticle(transform.position, "S_GoHome");
        _UIGameA.gameObject.SetActive(false);
        f1Object.SetActive(false);
        workBtn.SetActive(false);
        shopBtn.SetActive(false);
        homeBtn.SetActive(false);
        scoreBtn.SetActive(true);
        GameManager.instance.OnGoHome();
        cameraMain = homeCamera;
        CloseAll();
    }

    public void OnScore()
    {
        Cursor.SetCursor(cursorTextureOff, Vector2.zero, CursorMode.Auto);
        workBtn.SetActive(false);
        shopBtn.SetActive(false);
        homeBtn.SetActive(false);
        scoreBtn.SetActive(false);
        GameManager.instance.isTimesUp = true;
        cameraMain = scoreCamera;
        CloseAll();
        OnExitShoe();
    }

    void CloseAll()
    {
        workCamera.gameObject.SetActive(false);
        shopCamera.gameObject.SetActive(false);
        homeCamera.gameObject.SetActive(false);
        scoreCamera.gameObject.SetActive(false);

        cameraMain.gameObject.SetActive(true);
    }

    public void OnGameStart()
    {
        dayBG.SetActive(false);
        isDayShowUIClose = true;
        homeBtn.SetActive(true);
        shopBtn.SetActive(true);
        kakoAnimator.enabled = true;
        if (GameManager.instance.Day == 0)
        {
            GameManager.instance.ChangeScene("End");
        }
    }

    public void OnHomeLightOff()
    {
        homeLight.gameObject.SetActive(false);
    }

    public void OnClickF1Back()
    {
        if (mainEnum == ToolsEnum.Book && f1Object.activeSelf)
        {
            f1Object.SetActive(false);
            bookAni.SetBool("isBook", false);
            GameManager.instance.CreatParticle(transform.position, "S_Book");
            StartCoroutine(BookBack());
        }
    }

    public void OnCoin()
    {
        
        dialog.SetActive(!dialog.activeSelf);
    }

    IEnumerator BookBack()
    {
        yield return new WaitForSeconds(1f);
        mainEnum = ToolsEnum.None;
    }

}


