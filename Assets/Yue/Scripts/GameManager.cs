﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using System.IO;



public class GameManager : MonoBehaviour
{
    static public GameManager instance;

    public List<GameObject> particlePrefabList;
    List<string> particlePrefabStringList;

    public float dayTime = 60; //設定一天有幾秒
    public float DayTimeNow;

    public int Coin = 0;

    public int Day = 5;

    public Image timerImage;
    public Text coinText;

    public List<string> butItem;
    public int buyIndex;
    public bool isGoodEnd;

    private void Awake()
    {
        
        if (instance != null)
        {
            Destroy(this.gameObject);
            return;
            
        }

        DontDestroyOnLoad(this);

        if (instance == null)
            instance = this;
        buyIndex = -1;
        Coin = 0;
        butItem = new List<string>();
        CreateList();
    }

    public bool isTimesUp = false;
    public void SetTimer()
    {
        DayTimeNow = dayTime;
        isTimesUp = false;
    }

    public int goodEndCount = 0;
    public void OnGoHome()
    {
        isTimesUp = true;
        if (DayTimeNow < 90 && DayTimeNow > 45)
        {
            GameManager.instance.CreatParticle(transform.position, "S_GoodEnd" , 4);
            Debug.Log("Good");
            goodEndCount += 1;
        }
        else
        {
            ClickObject.instance.OnHomeLightOff();
        }
    }

    public void CoinAdd(int _coin)
    {
        Coin += _coin;
        CreatParticle(transform.position, "S_Coin");
    }

    private void Update()
    {
        if (Input.GetKeyDown("p"))
        {
            DayTimeNow -= 10;
        }

        if (Input.GetKeyDown("="))
        {
            CoinAdd(1000);
            ClickObject.instance.OnCoin();
            UpdateCoin();
        }
        if (Input.GetKeyDown("g"))
        {
            //isGoodEnd = true;
        }

        if (DayTimeNow > 0 && !isTimesUp)
        {
            DayTimeNow -= Time.deltaTime;
            timerImage.fillAmount = DayTimeNow / dayTime;
        }
        else
        {
            if(!isTimesUp)
            {
                isTimesUp = true;
                if(ClickObject.instance != null)
                {
                    ClickObject.instance.OnHome();
                    timerImage.fillAmount = 0;
                }
            }
        }
        
    }

    public void UpdateCoin()
    {
        coinText.text = "$" + Coin.ToString();
    }

    public string GetSceneNameNow()
    {
        return SceneManager.GetActiveScene().name;
    }

    public int GetSceneNameNowInt()
    {
        return System.Convert.ToInt32(GetSceneNameNow());
    }

    public void CreateSoundExample()
    {
        CreatParticle(transform.position, "S_Tool");
    }

    public void CreateList()
    {
        particlePrefabStringList = new List<string>();
        foreach (GameObject go in particlePrefabList)
        {
            particlePrefabStringList.Add(go.name);
        }
    }

    public void CreatParticle(Vector3 thePosition, string theName, float destroyTime = 3)
    {
        if (particlePrefabStringList.Contains(theName))
        {
            int index = particlePrefabStringList.IndexOf(theName);
            GameObject go = Instantiate(particlePrefabList[index], thePosition, transform.rotation) as GameObject;
            Destroy(go, destroyTime);
        }
    }

    public void CreatParticleWithTransform(Transform _transform, string theName, float destroyTime = 3)
    {
        if (particlePrefabStringList.Contains(theName))
        {
            int index = particlePrefabStringList.IndexOf(theName);
            GameObject go = Instantiate(particlePrefabList[index], new Vector3(_transform.position.x, _transform.position.y, -1), _transform.rotation) as GameObject;
            Destroy(go, destroyTime);
        }
    }


    public void ChangeScene(string _name)
    {
        
        Day -= 1;
        SceneManager.LoadScene(_name);
    }
}
    
