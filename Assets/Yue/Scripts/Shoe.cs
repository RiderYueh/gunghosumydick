﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoe : MonoBehaviour
{
    public GameObject oldBottom;
    public GameObject newBottom;
    private Rigidbody newBR;

    public int scoreA;
    public int scoreB;
    public int scoreC;

    private float gameSpeed = 0.3f;

    private int times = 5; //敲幾次

    private float endTime = 6; //小遊戲時間結束

    bool isTimesUp = false;
    float checkTimer = 1;
    bool isFail = false;

    private void Start()
    {
        newBottom.transform.parent = transform.parent;
        newBR = newBottom.GetComponent<Rigidbody>();
        scoreA = 150;
        scoreB = 100;
        scoreC = 0;
    }

    void OnScoreB()
    {
        Vector3 tempCenter = transform.GetChild(0).transform.position;
        tempCenter = new Vector3(tempCenter.x , newBottom.transform.position.y , tempCenter.z);

        scoreB = 100 - System.Convert.ToInt32(Vector3.Distance(tempCenter, newBottom.transform.position) * 500);
        Debug.Log(scoreB);
        if (scoreB <= 0) scoreB = 0;
        if (scoreB >= 98) scoreB = 100;
    }

    public void OnScoreC()
    {
        scoreC = Random.Range(20,100);
    }

    float flyTimer = 1;
    bool isRigidbodyMoving;
    bool isPlaySound;
    private void Update()
    {
        if(Input.GetKeyDown("q"))
        {
            //OnFix();
        }
       
        if (isFly)
        {
            if (flyTimer <= 0)
            {
                isFly = false;
                oldBottom.SetActive(false);
            }
            flyTimer -= Time.deltaTime;
            oldBottom.transform.localPosition -= new Vector3(0 , -Time.deltaTime * 0.1f , Time.deltaTime * 0.1f);
        }

        if(isPlayBottomGame)
        {
            endTime -= Time.deltaTime;

            ClickObject.instance.shoeTime3D.text = endTime.ToString("0");
            
            if (Input.GetMouseButton(0))
            {
                if (newBottom.transform.localPosition.x > 6.75f)
                {
                    newBottom.transform.localPosition += newBottom.transform.forward * Time.deltaTime * gameSpeed;
                }
                ClickObject.instance.bottomGameRightArror.sprite = ClickObject.instance.gSprite;
                
            }
            else
            {
                if (newBottom.transform.localPosition.x < 8.04f)
                {
                    newBottom.transform.localPosition -= newBottom.transform.forward * Time.deltaTime * gameSpeed;
                }
                ClickObject.instance.bottomGameRightArror.sprite = ClickObject.instance.wSprite;
            }

            if (Input.GetMouseButton(1))
            {
                if (newBottom.transform.localPosition.z > 0.2f)
                {
                    newBottom.transform.localPosition += newBottom.transform.right * Time.deltaTime * gameSpeed;
                }
                ClickObject.instance.bottomGameTopArror.sprite = ClickObject.instance.gSprite;
            }
            else
            {
                if (newBottom.transform.localPosition.z < 0.5f)
                {
                    newBottom.transform.localPosition += -newBottom.transform.right * Time.deltaTime * gameSpeed;
                }
                ClickObject.instance.bottomGameTopArror.sprite = ClickObject.instance.wSprite;
            }

            if (endTime <= 0)
            {
                isPlayBottomGame = false;
                isTimesUp = true;
            }

        }

        if(isTimesUp)
        {
            if (checkTimer <= 0)
                return;
            newBR.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
            checkTimer -= Time.deltaTime;
            if(newBR.velocity.y < -1)
            {
                isRigidbodyMoving = true;
            }
            if (newBR.velocity.y > -1 && isRigidbodyMoving && !isPlaySound)
            {
                isPlaySound = true;
                GameManager.instance.CreatParticle(transform.position, "S_Tool");

            }
            if (newBottom.transform.localPosition.y < -0.24f)
            {
                isFail = true;
                if (checkTimer <= 0)
                {
                    Debug.Log("黏失敗");
                    OnScoreB();
                    OnStickFail();
                }
            }
            if (newBottom.transform.localPosition.y < -0.07f)
            {
                
                if(checkTimer <= 0 && !isFail)
                {
                    Debug.Log("黏成功");
                    newBR.constraints = RigidbodyConstraints.FreezeAll;
                    OnScoreB();
                    OnStickSuccess();
                }
                
            }
        }
    }

    void OnStickFail()
    {
        newBottom.SetActive(false);
        ClickObject.instance.OnBottomGameEnd();
    }

    void OnStickSuccess()
    {
        newBottom.transform.parent = gameObject.transform;
        ClickObject.instance.OnBottomGameEnd();
    }

    public void OnFix()
    {
        if (isFly) return;
       oldBottom.transform.localPosition -= new Vector3(0, 0, 0.004f);
        times -= 1;
        if (times == 0) OnFly();
    }


    bool isFly = false;
    public void OnFly()
    {
        GameManager.instance.CreatParticle(transform.position, "S_BottomFly");
        isFly = true;
        ClickObject.instance.shoeEnum = ClickObject.ShoeState.HammerDone;
    }

    bool isPlayBottomGame = false;
    public void OnPlayBottomGame()
    {
        endTime = 6;
        newBottom.transform.localPosition = new Vector3(newBottom.transform.localPosition.x , 0.18f , newBottom.transform.localPosition.z);
        isPlayBottomGame = true;
        isTimesUp = false;
    }
}
