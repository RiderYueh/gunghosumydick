﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIGameA : MonoBehaviour
{
    public GameObject TAObject;
    public GameObject Mover;
    public float speed = 500;

    float posA = 135;

    float taPosA = 90;
    float taPosB = -40;

    bool isStart = false;

    void SetTaPos()
    {
        isWaiting = false;
        float tempPosY = Random.Range(taPosA , taPosB);
        TAObject.transform.localPosition = new Vector3(TAObject.transform.localPosition.x , tempPosY, TAObject.transform.localPosition.z);

        Mover.transform.localPosition = new Vector3(TAObject.transform.localPosition.x, -posA, TAObject.transform.localPosition.z);
        isMoverDown = false;
    }

    public void OnStart()
    {
        SetTaPos();
    }

    IEnumerator WaitToSetTaPos()
    {
        yield return new WaitForSeconds(0.4f);
        SetTaPos();
    }

    bool isWaiting = false;

    void Update()
    {
        if (isWaiting) return;

        if(Input.GetMouseButtonDown(0))
        {
            if(!isStart)
            {
                isStart = true;
                ClickObject.instance.OnHammerUsing();
            }

        }
        if(Input.GetMouseButtonUp(0))
        {
            if(isStart)
            {
                isStart = false;
                float taTopY = TAObject.transform.localPosition.y + 20;
                float taDownY = TAObject.transform.localPosition.y - 20;

                if (Mover.transform.localPosition.y < taTopY && Mover.transform.localPosition.y > taDownY)
                {
                    Debug.Log("Great");
                    ClickObject.instance.OnHammerGo();
                }
                else
                {
                    ClickObject.instance.OnHammerUsed();
                    Debug.Log("No");
                }
                isWaiting = true;
                StartCoroutine(WaitToSetTaPos());
            }
            
        }

        if(isStart)
        {
            MoveMover();
        }
    }


    bool isMoverDown = false;
    void MoveMover()
    {
        int reverse = 1;
        if(isMoverDown)
        {
            reverse = -1;
        }

        Mover.transform.localPosition += new Vector3(0, Time.deltaTime * speed * reverse, 0);


        if (Mover.transform.localPosition.y > posA && !isMoverDown)
        {
            isMoverDown = true;
        }
        else if(Mover.transform.localPosition.y < -posA && isMoverDown)
        {
            isMoverDown = false;
        }
    }
}














