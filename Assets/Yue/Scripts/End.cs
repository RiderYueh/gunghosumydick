﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class End : MonoBehaviour
{
    public List<Sprite> imageList;
    public List<Sprite> goodList;

    public Image mainImage;

    int counter = 0;
    private void Start()
    {
        if(GameManager.instance.isGoodEnd || GameManager.instance.goodEndCount > 4)
        {
            mainImage.sprite = goodList[counter];
        }
        else if(GameManager.instance.buyIndex >= 0)
        {
            mainImage.sprite = imageList[GameManager.instance.buyIndex];

        }

    }

    public void OnClickImage()
    {
        counter += 1;
        if (GameManager.instance.isGoodEnd && counter < 3)
        {
            mainImage.sprite = goodList[counter];

            if(counter == 2)
            {
                GameManager.instance.CreatParticle(transform.position, "S_GoodEnd2");
            }
        }
    }
}
